# Installation#

### Installation du d�p�t ###

Cloner le projet avec SourceTree ou suivre les �tapes suivantes :

* D�marrer Wamp ou Mamp.
* Lancer le terminal et choisir un dossier de reception.
* Ex�cuter la commande "cd " + "chemin de reception" dans le dossier Wamp ou Mamp.
* Pour cloner le projet, �x�cuter la commande dans le terminal "git clone lienduprojet"

### Installation du projet ###

* node.js est n�c�ssaire pour l'installation du projet.
* Si vous n'avez node.js voici un lien pour l'installer https://openclassrooms.com/courses/des-applications-ultra-rapides-avec-node-js/installer-node-js
* Dans PhpStorm, ouvrez le dossier frontExercise/dev.
* Dans la console de PhpStorm, �xecutez la commande "npm install", cette commande permet de lire le fichier "package.json" qui va contenir des modules.

### Configurer le d�p�t ###

* Acc�dez au fichier dev/gulp/config.js pour configurer le module browserSync (il permet de rafra�chir le navigateur quand on modifie un fichier scss, js ou php)
* Sur la ligne 22 il faut modifier le proxy nommez gobelins.localhost.<br/>
* Si vous �tes sur Mac lancer Mamp cliquez sur hosts : Ajouter un host gobelins.localhost Puis indiquez le chemin du projet "frontExercise/dev" V�rifiez bien que vous avec bien configurer vos ports en cliquant sur Ports > Set ports to 80, 81, 443, 7443 & 3306
* Si vous �tes sur Windows lancer Wamp ajoutez un host en cliquand sur Wamp>Vos VirtualsHosts gestion VirtualHost <br/> * Suivez les m�mes �tapes que Mamp

### Lancer le projet ###

* Dans le terminal, executez cd frontExercise/dev
* Executez npm start
* Acc�dez � http://gobelins.localhost/
