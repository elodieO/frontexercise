export default {

    el : document.createElement('div'),

    dispatch: function(e, datas){
        let event = new CustomEvent(e, datas || {});
        this.el.dispatchEvent(event);
    },

    listen: function(e, method){
        this.el.addEventListener(e, method);
    }
}