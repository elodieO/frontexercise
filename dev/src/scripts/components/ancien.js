    // /**************   CONSTRUCTION SLIDER   *************************/

    // var SliderItem = function(index, data) {
    //     this.index = index;
    //     this.data = data;
    // };


    // SliderItem.prototype.createHTML = function() {
    //
    //     var li = document.createElement('li');
    //     li.classList.add('slider__item');
    //     li.classList.add('slider__item-'+this.index);
    //     li.setAttribute('data-id', this.index);
    //
    //     var article = document.createElement('article');
    //     article.classList.add('slide__content');
    //
    //     if(this.index > 0)
    //         li.classList.add('hidden');
    //
    //     var divImage = document.createElement('div');
    //     divImage.classList.add('content__image');
    //
    //     var div = document.createElement('div');
    //
    //     var image = document.createElement('img');
    //     image.src = "img/"+this.data.url;
    //
    //     var divDescription = document.createElement('div');
    //     divDescription.classList.add('content__text');
    //
    //     var title = document.createElement('h1');
    //     title.classList.add('content__h1');
    //     title.innerText = this.data.title;
    //
    //     var description = document.createElement('p');
    //     description.classList.add('content__p');
    //     description.innerText = this.data.description;
    //
    //     var link = document.createElement('button');
    //     link.classList.add('content__button');
    //     link.href = this.data.url;
    //     link.innerHTML = "commander";
    //
    //     divDescription.appendChild(title);
    //     divDescription.appendChild(description);
    //     divDescription.appendChild(link);
    //
    //     divImage.appendChild(div);
    //     div.appendChild(image);
    //
    //     article.appendChild(divImage);
    //     article.appendChild(divDescription);
    //
    //     li.appendChild(article);
    //
    //     this.el = li;
    //
    //     return li;
    //
    // };

    // SliderItem.prototype.createBullet = function() {
    //     var li = document.createElement('li');
    //
    //     var link = document.createElement('a');
    //     link.href = "#";
    //
    //     var div = document.createElement('div');
    //     div.classList.add('nav__link');
    //     div.classList.add('nav__link-' + (this.index));
    //
    //     var img = document.createElement('img');
    //     img.src = "img/"+this.data.url;
    //
    //     if(this.index === 0)
    //         div.classList.add('nav__link--active');
    //
    //     div.appendChild(img);
    //     link.appendChild(div);
    //     li.appendChild(link);
    //
    //     return li;
    // };




    // var Slider = function(el) {
    //     this.el = el;
    //     this.slides = [];
    //     this.bullets = []
    //     this.currentIndex = 0;
    //     this.init();
    // };

//
//     Slider.prototype.init = function () {
//         this.bulletsContainer = this.el.querySelector('.nav__buttons');
//         this.sliderContainer = this.el.querySelector('.slider__slides');
//
//         this.left = this.el.querySelector('.arrow-left');
//         this.right = this.el.querySelector('.arrow-right');
//
//         this.listen();
//     };

// Slider.prototype.loadContent = function() {
//     var req = new XMLHttpRequest();
//     req.open('GET', "./objet.json", true);
//     req.addEventListener('readystatechange', this.loaded.bind(this, req));
//     req.send();
// };
//
// Slider.prototype.loaded = function(req) {
//     if(req.readyState == XMLHttpRequest.DONE && req.status == 200) {
//         this.appendInPage(JSON.parse(req.responseText));
//     }
// };
//
// Slider.prototype.listen = function(index) {
//     this.left.addEventListener('click', this.clicked.bind(this, -1));
//     this.right.addEventListener('click', this.clicked.bind(this, 1));
//     this.bullets[index].addEventListener('click', this.clickedBullet.bind(this, index));
//     //this.bullet1.addEventListener('click', this.clickedBullet.bind(this, 0));
// };


// Slider.prototype.clicked = function(direction) {
//     var Mytimeline = new TimelineMax();
//     var slideEl = this.slides[this.currentIndex].el;
//     var image = slideEl.querySelector('img');
//     var description = slideEl.querySelector('.content__text');
//     var div = slideEl.querySelector('.content__image>div');
//
//     var next = (this.currentIndex + direction + this.slides.length) % this.slides.length;
//     var nextSlide = this.slides[next].el;
//     var imageNext = this.slides[next].el.querySelector('img');
//     var descriptionNext = this.slides[next].el.querySelector('.content__text');
//     var divNext = this.slides[next].el.querySelector('.content__image>div');
//
//     Mytimeline.to(div, 0.2, {
//
//         top:"-30px",
//         left: "30px"
//     });
//
//     Mytimeline.to(image, 0.2, {
//         top:"180",
//         opacity: "0",
//         onComplete: function() {
//             nextSlide.classList.remove('hidden');
//             slideEl.classList.add('hidden');
//             this.bulletsContainer.querySelector('.nav__link--active').classList.remove('nav__link--active');
//             this.bulletsContainer.querySelectorAll('.nav__link')[next].classList.add('nav__link--active');
//             image.style.top= "-180px";
//         }.bind(this)
//     });
//
//     Mytimeline.to(imageNext, 0.2, {
//         top:"0",
//         opacity :"1"
//     });
//
//     Mytimeline.to(description, 0.15, {
//         top:"180",
//         opacity: "0",
//         onComplete: function(){
//             description.style.top= "-180px";
//         }
//     }, '-=0.6');
//
//     Mytimeline.to(divNext, 0.1, {
//         top:"0",
//         left: "0"
//     });
//
//     Mytimeline.to(descriptionNext, 0.15, {
//         top:"0",
//         opacity: "1"
//     });
//
//     this.currentIndex = next;
//
//
// };


// Slider.prototype.clickedBullet = function(index) {
//     console.log('bonjour voici');
//
//
// };

//
// Slider.prototype.appendInPage = function(data) {
//     var i = 0;
//     var l = data.length;
//
//     for(i; i < l; i++) {
//         this.slides[i] = new SliderItem(i, data[i]);
//         this.sliderContainer.appendChild(this.slides[i].createHTML());
//         this.bulletsContainer.appendChild(this.slides[i].createBullet());
//         this.bullets[i] = this.el.querySelector('.nav__link-'+i);
//         console.log(this.bullets[i]);
//         this.listen(i);
//     }
// };

//
// slider.loadContent();
// slider.appendInPage();

}
//
// if(document.readyState != 'loading') {
//     ready();
// } else {
//     document.addEventListener('DOMContentLoaded', ready);
// }