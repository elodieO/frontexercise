class Product {

    constructor(index, datas) {

        this.index = index;
        this.datas = datas;

    }

    buildBestSellers(data){

        let li = document.createElement('li');
        li.classList.add('products__item');

        if (data.stock < 2){
            li.classList.add('low_stock');
        }

        let article = document.createElement('article');
        article.classList.add('product');

        let header = document.createElement('header');
        header.classList.add('product__header');

        let divImage = document.createElement('div');
        divImage.classList.add('product__img');

        let image = document.createElement('img');
        image.classList.add('center-horizontal');
        image.src = data.image;

        let title = document.createElement('h1');
        title.classList.add('product__h1');
        title.innerText = data.title;

        let description = document.createElement('p');
        description.classList.add('product__p');
        description.innerText = data.Description;

        let footer = document.createElement('footer');
        footer.classList.add('product__footer');

        let stock = document.createElement('p');
        stock.classList.add('product__p');
        stock.innerText = data.stock + ' en stock';

        divImage.appendChild(image);

        header.appendChild(divImage);
        header.appendChild(title);
        header.appendChild(description);

        footer.appendChild(stock);

        article.appendChild(header);
        article.appendChild(footer);

        li.appendChild(article);

        this.el = li;

        return li;
    }

    buildAllProducts(data){

        let li = document.createElement('li');
        li.classList.add('products__item');

        if (data.stock < 2){
            li.classList.add('low_stock');
        }

        let article = document.createElement('article');
        article.classList.add('product');

        let header = document.createElement('header');
        header.classList.add('product__header');

        let divImage = document.createElement('div');
        divImage.classList.add('product__img');

        let image = document.createElement('img');
        image.classList.add('center-horizontal');
        image.src = data.image;

        let title = document.createElement('h1');
        title.classList.add('product__h1');
        title.innerText = data.title;

        let description = document.createElement('p');
        description.classList.add('product__p');
        description.innerText = data.Description;

        let footer = document.createElement('footer');
        footer.classList.add('product__footer');

        let stock = document.createElement('p');
        stock.classList.add('product__p');
        stock.innerText = data.stock + ' en stock';

        divImage.appendChild(image);

        header.appendChild(divImage);
        header.appendChild(title);
        header.appendChild(description);

        footer.appendChild(stock);

        article.appendChild(header);
        article.appendChild(footer);

        li.appendChild(article);

        this.el = li;

        return li;
    }

}

export default Product;