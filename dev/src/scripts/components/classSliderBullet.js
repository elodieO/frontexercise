import BUS from './BUS';

class SliderBullet {

    constructor(index, datas) {

        this.index = index;
        this.datas = datas;

    }

    buildBullet(data, index){
        let li = document.createElement('li');

        let div = document.createElement('div');
        div.classList.add('nav__link');
        div.classList.add('nav__link-' + (index));

        let img = document.createElement('img');
        img.src = data.image;

        if(this.index === 0)
            div.classList.add('nav__link--active');

        div.appendChild(img);
        li.appendChild(div);

        this.id = index;
        this.data = data;
        div.addEventListener('click', () => this.click());
        return li;
    }

    listen(){

    }

    click(){
        console.log(this.id);
        BUS.dispatch('bulletMove',{detail: {bullet: this.id}});
    }
}

export default SliderBullet;