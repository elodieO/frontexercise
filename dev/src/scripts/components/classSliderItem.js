class SliderItem {

    constructor(index, datas) {

        this.index = index;
        this.datas = datas;

    }

    buildItem(data, index){

        let li = document.createElement('li');
        li.classList.add('slider__item');
        li.classList.add('slider__item-'+index);
        li.setAttribute('data-id', index);

        let article = document.createElement('article');
        article.classList.add('slide__content');

        if(this.index > 0)
            li.classList.add('hidden');

        let divImage = document.createElement('div');
        divImage.classList.add('content__image');

        let div = document.createElement('div');

        let image = document.createElement('img');
        image.src = data.image;

        let divDescription = document.createElement('div');
        divDescription.classList.add('content__text');

        let title = document.createElement('h1');
        title.classList.add('content__h1');
        title.innerText = data.title;

        let description = document.createElement('p');
        description.classList.add('content__p');
        description.innerText = data.Description;

        let link = document.createElement('button');
        link.classList.add('content__button');
        link.href = data.url;
        link.innerHTML = "commander";

        divDescription.appendChild(title);
        divDescription.appendChild(description);
        divDescription.appendChild(link);

        divImage.appendChild(div);
        div.appendChild(image);

        article.appendChild(divImage);
        article.appendChild(divDescription);

        li.appendChild(article);

        this.el = li;

        return li;
    }

    listen(){

    }

    click(e){
        e.preventDefault();
        BUS.dispatch();
    }
}

export default SliderItem;