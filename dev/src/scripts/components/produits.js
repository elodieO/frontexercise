//function ready() {

import Product from './classProduit';

class lesProduits {

    constructor(el1 ,el2, url) {

        this.el1 = el1;
        this.el2 = el2;
        this.url = url;
        this.products = [];

        this.init(this.el1, this.el2);
        this.load(url);
        console.log('bonjour');

    }

    load(url){

        let req = new XMLHttpRequest();
        req.open('GET', url, true);
        console.log(req);
        req.addEventListener('readystatechange', this.loaded.bind(this, req));
        req.send();

    }

    loaded(req){

        if(req.readyState === XMLHttpRequest.DONE && (req.status === 200 || req.status === 0)) {
            this.build(JSON.parse(req.responseText));
        }

    }

    init(el1, el2){

        this.bestSellersContainer = el1.querySelector('.products__list');
        this.allProductsContainer = el2.querySelector('.products__list');

    }

    build(data){

        console.log(data);
        let i = 0;
        let l = data.length;

        for(i; i < l; i++) {

            this.products[i] = new Product(i, data[i]);
            this.bestSellersContainer.appendChild(this.products[i].buildBestSellers(data[i]));
            this.allProductsContainer.appendChild(this.products[i].buildAllProducts(data[i]));
        }
    }

}


//}

let MesProduits = new lesProduits(document.querySelector('.bestseller'), document.querySelector('.all'), "scripts/produit.json");