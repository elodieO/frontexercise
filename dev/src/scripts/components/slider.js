//function ready() {

    import BUS from './BUS';
    import SliderBullet from './classSliderBullet';
    import SliderItem from './classSliderItem';


class Slider {

    constructor(el, url) {

        this.el = el;
        this.url = url;
        this.slides = [];
        this.bullets = []
        this.currentIndex = 0;
        this.max = 0;

        this.init(this.el);
        this.load(url);

    }

    load(url){

        let req = new XMLHttpRequest();
        req.open('GET', url, true);
        console.log(req);
        req.addEventListener('readystatechange', this.loaded.bind(this, req));
        req.send();

    }

    loaded(req){

        if(req.readyState === XMLHttpRequest.DONE && (req.status === 200 || req.status === 0)) {
            this.build(JSON.parse(req.responseText));
        }

    }

    init(el){

        this.bulletsContainer = el.querySelector('.nav__buttons');
        this.sliderContainer = el.querySelector('.slider__slides');

        this.left = el.querySelector('.arrow-left');
        this.right = el.querySelector('.arrow-right');

        this.listen();

    }


    listen(index){
        console.log('je suis là');
        console.log(index);
        this.left.addEventListener('click', this.clicked.bind(this, -1));
        this.right.addEventListener('click', this.clicked.bind(this, 1));
        BUS.listen('bulletMove', (e) => this.bulletMove(e));
    }

    bulletMove(e) {
        console.log(e.detail.bullet);
        this.clicked(e.detail.bullet - this.currentIndex);
    }

    clicked(direction){

        let Mytimeline = new TimelineMax();

        let slideEl = this.slides[this.currentIndex].el;
        let image = slideEl.querySelector('img');
        let description = slideEl.querySelector('.content__text');
        let div = slideEl.querySelector('.content__image>div');


        let next = (this.currentIndex + direction + this.slides.length) % this.slides.length;
        console.log(next);
        let nextSlide = this.slides[next].el;
        let imageNext = this.slides[next].el.querySelector('img');
        let descriptionNext = this.slides[next].el.querySelector('.content__text');
        let divNext = this.slides[next].el.querySelector('.content__image>div');

        Mytimeline.to(div, 0.2, {

            top:"-30px",
            left: "30px"
        });

        Mytimeline.to(image, 0.2, {
            top:"180",
            opacity: "0",
            onComplete: function() {
                nextSlide.classList.remove('hidden');
                slideEl.classList.add('hidden');
                this.bulletsContainer.querySelector('.nav__link--active').classList.remove('nav__link--active');
                this.bulletsContainer.querySelectorAll('.nav__link')[next].classList.add('nav__link--active');
                image.style.top= "-180px";
            }.bind(this)
        });

        Mytimeline.to(imageNext, 0.2, {
            top:"0",
            opacity :"1"
        });

        Mytimeline.to(description, 0.15, {
            top:"180",
            opacity: "0",
            onComplete: function(){
                description.style.top= "-180px";
            }
        }, '-=0.6');

        Mytimeline.to(divNext, 0.1, {
            top:"0",
            left: "0"
        });

        Mytimeline.to(descriptionNext, 0.15, {
            top:"0",
            opacity: "1"
        });

        this.currentIndex = next;

    }

    build(data){
        let i = 0;
        let l = data.length;

        for(i; i < l; i++) {
            console.log(this.sliderContainer);
            console.log(this.bulletsContainer);
            this.slides[i] = new SliderItem(i, data[i]);
            this.bullets[i] = new SliderBullet(i, data[i]);
            this.sliderContainer.appendChild(this.slides[i].buildItem(data[i], i));
            this.bulletsContainer.appendChild(this.bullets[i].buildBullet(data[i], i));
            this.bullets[i] = this.el.querySelector('.nav__link-'+i);
            //console.log(this.bullets[i]);
            //this.listen(i);
        }
    }

    move(dir) {

        console.log(dir);


        this.slides[this.currentIndex].el.hide(dir);
        this.bullets[this.currentIndex].el.hide(dir);

        this.currentIndex = Math.max(-1, Math.min(this.max, this.currentIndex + dir));
        if (this.currentIndex === this.max) {
            this.currentIndex = 0;
        } else if(this.currentIndex === -1) {
            this.currentIndex = this.max - 1;
        }
        this.bullets[this.currentIndex].show(dir);
        this.slides[this.currentIndex].show(dir);

    }

    hide(dir){
        console.log(dir);
    }

    show(dir){
        console.log(dir);
    }


}


//}

let slider = new Slider(document.querySelector('.slider'), "scripts/objet.json");